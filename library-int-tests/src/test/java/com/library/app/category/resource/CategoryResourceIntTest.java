package com.library.app.category.resource;

import static com.library.app.commontests.category.CategoryForTestsRepository.*;
import static com.library.app.commontests.utils.FileTestNameUtils.*;
import static com.library.app.commontests.utils.JsonTestUtils.*;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.json.*;
import javax.ws.rs.core.*;

import org.apache.http.*;
import org.hamcrest.*;
import org.jboss.arquillian.container.test.api.*;
import org.jboss.arquillian.junit.*;
import org.jboss.arquillian.test.api.*;
import org.jboss.shrinkwrap.api.*;
import org.jboss.shrinkwrap.api.asset.*;
import org.jboss.shrinkwrap.api.spec.*;
import org.jboss.shrinkwrap.resolver.api.maven.*;
import org.junit.*;
import org.junit.runner.*;

import com.fasterxml.jackson.databind.*;
import com.library.app.category.model.*;
import com.library.app.common.json.*;
import com.library.app.commontests.utils.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class CategoryResourceIntTest {
	
	@ArquillianResource
	private URL url;
	
	private ResourceClient resourceClient;
	
	private static String PATH_RESOURCE = ResourceDefinitions.CATEGORY.getResourceName();

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.library.app")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", "org.mockito:mockito-core:1.9.5")
								.withTransitivity().asFile());
	}
	
	@Before
	public void initTestCase(){
		this.resourceClient = new ResourceClient(url);
		resourceClient.resourcePath("/DB").delete();
	}
	
	@Test
	@RunAsClient
	public void addValidCategoryWithId(){
		Long id = addCategoryAndGetId("category.json");
		findCategoryAndAssertResponseWithCategory(id,java());
	}
	
	@Test
	@RunAsClient
	public void addCategoryWithNullName(){
		Response response = resourceClient.resourcePath(PATH_RESOURCE).postWithFile(getPathFileRequest(PATH_RESOURCE, "categoryWithNullName.json"));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryErrorNullName.json");
	}
	
	@Test
	@RunAsClient
	public void addExistentCategory(){
		resourceClient.resourcePath(PATH_RESOURCE).postWithFile(getPathFileRequest(PATH_RESOURCE,"category.json"));
		
		Response response = resourceClient.resourcePath(PATH_RESOURCE).postWithFile(getPathFileRequest(PATH_RESOURCE,"category.json"));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryAlreadyExist.json");
		
	}
	
	@Test
	@RunAsClient
	public void updateValidCategory(){
		Long id = addCategoryAndGetId("category.json");
		findCategoryAndAssertResponseWithCategory(id, java());
		
		Response response = resourceClient.resourcePath(PATH_RESOURCE+"/"+id).putWithFile(getPathFileRequest(PATH_RESOURCE,"categoryCleanCode.json"));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
		findCategoryAndAssertResponseWithCategory(id, cleanCode());
	}
	
	@Test
	@RunAsClient
	public void updateWithNameOfOtherCategory(){
		Long javaCategoryId = addCategoryAndGetId("category.json");
		addCategoryAndGetId("categoryCleanCode.json");
		
		Response response = resourceClient.resourcePath(PATH_RESOURCE+"/"+javaCategoryId).putWithFile(getPathFileRequest(PATH_RESOURCE,"categoryCleanCode.json"));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryAlreadyExist.json");
		
	}
	
	@Test
	@RunAsClient
	public void updateCategoryNotFound(){
		Response response = resourceClient.resourcePath(PATH_RESOURCE+"/999").putWithFile(getPathFileRequest(PATH_RESOURCE,"category.json"));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_NOT_FOUND)));
	}
	
	@Test
	@RunAsClient
	public void findCategoryNotFound(){
		Response response = resourceClient.resourcePath(PATH_RESOURCE+"/999").get();
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_NOT_FOUND)));
	}
	
	@Test
	@RunAsClient
	public void findAll(){
		resourceClient.resourcePath("DB/"+PATH_RESOURCE).postWithContent("");
		
		Response response = resourceClient.resourcePath(PATH_RESOURCE).get();
		
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
		assertResponseContainsTheCategories(response, 4, architecture(), cleanCode(), java(), networks());
		
	}

	private void assertResponseContainsTheCategories(Response response, Integer expectedTotalRecords, Category...expectedCategories) {
		JsonObject result = JsonReaderUtil.readAsJsonObject(response.readEntity(String.class));
		Integer totalRecords = result.getJsonObject("paging").getInt("totalRecords");
		assertThat(totalRecords, is(equalTo(expectedTotalRecords)));
		
		JsonArray categories = result.getJsonArray("entries");
		assertThat(categories, hasSize(expectedTotalRecords));
		
		List<String> receivedNames = new ArrayList<>();
		for(int i =0; i< categories.size(); i++){
			receivedNames.add(categories.getJsonObject(i).getString("name"));
		}
		
		List<String> expectedNames = new ArrayList<>();
		for(int i =0; i< expectedCategories.length; i++){
			expectedNames.add(expectedCategories[i].getName());
		}
		
		assertThat(receivedNames, contains(expectedNames.toArray(new String[expectedNames.size()])));
		
	}

	private void assertJsonResponseWithFile(Response response, String fileName) {
		assertJsonMatchesFileContent(response.readEntity(String.class),getPathFileResponse(PATH_RESOURCE, fileName));
		
	}
	
	private Long addCategoryAndGetId(String fileName){
	 return IntTestUtils.addElementWithFileAndGetId(resourceClient, PATH_RESOURCE, PATH_RESOURCE, fileName);
	}
	
	private void findCategoryAndAssertResponseWithCategory(Long categoryIdToBeFound, Category expectedCategory){
		String json = IntTestUtils.findById(resourceClient, PATH_RESOURCE, categoryIdToBeFound);
		
		JsonObject readObject = JsonReaderUtil.readAsJsonObject(json);
		assertThat(readObject.getString("name"),is(equalTo(expectedCategory.getName())));
	}

}
