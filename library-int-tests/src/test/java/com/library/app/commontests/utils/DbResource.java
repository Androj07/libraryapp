package com.library.app.commontests.utils;

import javax.decorator.*;
import javax.inject.*;
import javax.ws.rs.*;

@Path("/DB")
public class DbResource {
	
	@Inject
	private TestRepositoryEJB testRepo;

	@DELETE
	public void deleteAll(){
		testRepo.deleteAll();
	}
}
