package com.library.app.commontests.utils;
import static com.library.app.commontests.utils.JsonTestUtils.*;
import java.net.*;

import javax.ws.rs.client.*;
import javax.ws.rs.client.Invocation.*;
import javax.ws.rs.core.*;

import org.apache.commons.lang3.*;

public class ResourceClient {
	private URL urlBase;
	private String resourcePath;
	
	public ResourceClient(URL urlBase){
		this.urlBase = urlBase;
	}
	
	public ResourceClient resourcePath(String resourcePath){
		this.resourcePath = resourcePath;
		return this;
	}
	
	public Response postWithFile(String fileName){
		return postWithContent(getRequestFromFileOrEmptyIfNullFile(fileName));
		
	}

	public Response postWithContent(String content) {
		return buildClient().post(Entity.entity(content, MediaType.APPLICATION_JSON));
	}
	
	public Response get(){
		return buildClient().get();
	}
	
	private Builder buildClient(){
		Client resourceClient = ClientBuilder.newClient();
		return resourceClient.target(getFullURL(resourcePath)).request();
	}

	private String getFullURL(String resourcePath) {
		try {
			return this.urlBase.toURI() + "api/" + resourcePath;
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private String getRequestFromFileOrEmptyIfNullFile(String fileName) {
		if(fileName == null){
			return StringUtils.EMPTY;
		}
		return readJsonFile(fileName);
	}

	public Response putWithFile(String fileName) {
		return putWithContent(getRequestFromFileOrEmptyIfNullFile(fileName));
	}
	private Response putWithContent(String content) {
		return buildClient().put(Entity.entity(content, MediaType.APPLICATION_JSON)); 
	}

	public void delete() {
		buildClient().delete();
	}

}
