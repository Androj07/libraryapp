package com.library.app.commontests.utils;

import static com.library.app.commontests.utils.FileTestNameUtils.*;

import javax.ws.rs.core.*;

import org.apache.http.*;
import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

@Ignore
public class IntTestUtils {
	public static Long addElementWithFileAndGetId(ResourceClient resourceClient, String pathResource, String mainFolder, String fileName){
		Response response = resourceClient.resourcePath(pathResource).postWithFile(getPathFileRequest(mainFolder,fileName));
		return assertResponseIsCreatedAndGetId(response);
	}

	private static Long assertResponseIsCreatedAndGetId(Response response) {
		assertThat(response.getStatus(),is(equalTo(HttpStatus.SC_CREATED)));
		Long id = JsonTestUtils.getIdFromJson(response.readEntity(String.class));
		return id;
	}
	
	public static String findById(ResourceClient resourceClient, String pathResource, Long id){
		Response response = resourceClient.resourcePath(pathResource + "/" + id).get();
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
		return response.readEntity(String.class);
	}
}
