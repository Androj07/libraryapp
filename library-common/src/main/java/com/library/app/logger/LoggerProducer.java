package com.library.app.logger;

import javax.enterprise.inject.*;
import javax.enterprise.inject.spi.*;

import org.slf4j.*;

public class LoggerProducer {
	
	@Produces
	public Logger produceLogger(InjectionPoint iPoint){
		return LoggerFactory.getLogger(iPoint.getMember().getDeclaringClass());
	}

}
