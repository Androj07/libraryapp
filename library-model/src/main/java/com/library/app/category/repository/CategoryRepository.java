package com.library.app.category.repository;

import java.util.List;

import javax.ejb.*;
import javax.enterprise.context.*;
import javax.persistence.*;

import com.library.app.category.model.Category;

//@Stateless
@RequestScoped
public class CategoryRepository {

	@PersistenceContext
	EntityManager em;

	public Category add(final Category category) {
		em.persist(category);
		return category;
	}

	public Category findById(final Long id) {
		if(id == null){
			return null;
		}
		return em.find(Category.class, id);
	}

	public void update(Category category) {
		em.merge(category);
		
	}

	public List<Category> findAll(String orderField) {
		TypedQuery<Category> query = em.createQuery("SELECT c FROM Category c ORDER BY c." + orderField,Category.class);
		return query.getResultList();
	}

	public boolean alreadyExists(Category category) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT 1 FROM Category c where c.name = :name");
		if(category.getId() != null){
			jpql.append(" and c.id != :id");
		}
		
		Query query = em.createQuery(jpql.toString());
		query.setParameter("name", category.getName());
		if(category.getId() != null){
			query.setParameter("id", category.getId());
		}
		
		return query.setMaxResults(1).getResultList().size() > 0;
	}

	public boolean existsById(Long id) {
		return em.createQuery("SELECT c FROM Category c WHERE id = :id")
				.setParameter("id", id)
				.setMaxResults(1)
				.getResultList().size() > 0;
	}

}