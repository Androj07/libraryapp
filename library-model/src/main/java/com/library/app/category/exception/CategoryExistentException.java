package com.library.app.category.exception;

import javax.ejb.*;

@ApplicationException
public class CategoryExistentException extends RuntimeException{
	private static final long serialVersionUID = 1L;
}
