package com.library.app.category.services;

import java.util.List;

import javax.ejb.*;

import com.library.app.category.model.Category;

@Local
public interface CategoryServices {
	Category add(Category category);
	void update(Category category);
	Category findById(Long id);
	List<Category> findAll();
}
