package com.library.app.commontests.utils;

import java.util.*;

import javax.ejb.*;
import javax.persistence.*;

import org.junit.*;

import com.library.app.category.model.*;

@Ignore
@Stateless
public class TestRepositoryEJB {
	
	@PersistenceContext
	private EntityManager em;
	
	private static final List<Class<?>> ENTITIES_TO_REMOVE = Arrays.asList(Category.class);

	public void deleteAll(){
		for(Class<?> entityClass : ENTITIES_TO_REMOVE){
			deleteAllForEntity(entityClass);
		}
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	private void deleteAllForEntity(Class<?> entityClass){
		List<Object> rows = em.createQuery("Select e From "+entityClass.getSimpleName()+" e").getResultList();
		for(Object row : rows){
			em.remove(row);
		}
	}
}
