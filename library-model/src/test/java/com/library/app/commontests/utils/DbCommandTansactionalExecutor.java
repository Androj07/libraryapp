package com.library.app.commontests.utils;

import javax.persistence.EntityManager;

import org.junit.Ignore;

@Ignore
public class DbCommandTansactionalExecutor {
	private EntityManager em;
	
	public DbCommandTansactionalExecutor(EntityManager em){
		this.em = em;
	}
	
	public <T> T executeCommand(DbCommand<T> dbCommand){
		try{
			em.getTransaction().begin();
			T toReturn = dbCommand.execute();
			em.getTransaction().commit();
			em.clear();
			return toReturn;
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			throw new IllegalStateException(e);
		}
	}

}
