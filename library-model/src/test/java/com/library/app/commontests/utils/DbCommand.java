package com.library.app.commontests.utils;

public interface DbCommand<T> {
	T execute();
}
