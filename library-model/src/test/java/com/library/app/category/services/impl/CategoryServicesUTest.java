package com.library.app.category.services.impl;
import static com.library.app.commontests.category.CategoryForTestsRepository.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Validation;
import javax.validation.Validator;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.library.app.category.exception.CategoryExistentException;
import com.library.app.category.exception.CategoryNotFoundException;
import com.library.app.category.model.Category;
import com.library.app.category.repository.CategoryRepository;
import com.library.app.common.exception.FieldNotValidException;
@RunWith(MockitoJUnitRunner.class)
public class CategoryServicesUTest {
	@InjectMocks
	private CategoryServicesImpl categoryServices;
	
	@Spy
	private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	
	@Mock
	private CategoryRepository categoryRepository;
	
	@Before
	public void intTestCase() {
	}
	
	@Test
	public void addCategoryWithNullName(){
		try{
			categoryServices.add(new Category());
			fail("Exception should have been thrown");
		}catch(FieldNotValidException e){
			assertThat(e.getFieldName(), is(equalTo("name")));
		}
	}
	
	@Test
	public void addCategoryWithTooLongName(){
		addCategoryWithInvalidName("to loong name for category");
	}
	
	@Test
	public void addCategoryWithTooShortName(){
		addCategoryWithInvalidName("t");
	}
	
	@Test(expected = CategoryExistentException.class)
	public void addCategoryWithExistentName(){
		when(categoryRepository.alreadyExists(java())).thenReturn(true);
		
		categoryServices.add(java());
	}
	
	@Test
	public void addValidCategory() {
		when(categoryRepository.alreadyExists(java())).thenReturn(false);
		when(categoryRepository.add(java())).thenReturn(getCategoryWithId(java(), 1L));
		Category category = categoryServices.add(java());
		assertThat(category, is(equalTo(getCategoryWithId(java(), 1L))));
	}
	
	private void addCategoryWithInvalidName(String name){
		try{
			categoryServices.add(new Category(name));
			fail("Exception should have been thrown");
		}catch(FieldNotValidException e){
			assertThat(e.getFieldName(), is(equalTo("name")));
		}
	}
	
	@Test
	public void updateCategoryWithNullName(){
		updateCategoryWithInvalidName(null);
	}
	@Test
	public void updateCategoryTooSchortName(){
		updateCategoryWithInvalidName("A");
	}
	@Test
	public void updateCategoryWithTooLongNameName(){
		updateCategoryWithInvalidName("Category with too long name ");
	}
	
	@Test(expected = CategoryExistentException.class)
	public void updateCategoryWithExistentName(){
		when(categoryRepository.alreadyExists(java())).thenReturn(true);
		
		categoryServices.update(java());
	}
	
	@Test
	public void updateValidCategory(){
		when(categoryRepository.alreadyExists(java())).thenReturn(false);
		when(categoryRepository.existsById(1L)).thenReturn(true);
		
		categoryServices.update(getCategoryWithId(java(), 1L));
		
		verify(categoryRepository).update(getCategoryWithId(java(), 1L));
		
	}
	
	
	private void updateCategoryWithInvalidName(String name){
		try{
			categoryServices.update(new Category(name));
			fail("Exception should have been thrown");
		}catch(FieldNotValidException e){
			assertThat(e.getFieldName(), is(equalTo("name")));
		}
	}
	
	@Test
	public void findCategoryById(){
		when(categoryRepository.findById(1L)).thenReturn(getCategoryWithId(java(), 1L));
		
		Category result = categoryServices.findById(1L);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getId(), is(equalTo(1L)));
		assertThat(result.getName(), is(equalTo(java().getName())));
	}
	
	@Test(expected = CategoryNotFoundException.class)
	public void findCategoryByIdNotFound(){
		when(categoryRepository.findById(1L)).thenReturn(null);
		
		categoryServices.findById(1L);
		
	}
	
	@Test
	public void findAllNoCategories(){
		when(categoryRepository.findAll(anyString())).thenReturn(new ArrayList<>());
		
		List<Category> categories = categoryServices.findAll();
		
		assertThat(categories, hasSize(0));
	}
	
	@Test
	public void findAll(){
		when(categoryRepository.findAll(anyString())).thenReturn(allCategories());
		
		List<Category> categories = categoryServices.findAll();
		
		assertThat(categories, is(equalTo(allCategories())));
	}
	
}
