package com.library.app.category.repository;

import static com.library.app.commontests.category.CategoryForTestsRepository.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

import java.util.List;

import javax.naming.CommunicationException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.swing.JColorChooser;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import com.library.app.category.model.Category;
import com.library.app.commontests.utils.DbCommand;
import com.library.app.commontests.utils.DbCommandTansactionalExecutor;

public class CategoryRepositoryUTest {
	private EntityManagerFactory emf;
	private EntityManager em;
	private CategoryRepository categoryRepository;
	private DbCommandTansactionalExecutor commandExecutor;

	@Before
	public void initTestCase() {
		emf = Persistence.createEntityManagerFactory("libraryPU");
		em = emf.createEntityManager();
		commandExecutor = new DbCommandTansactionalExecutor(em);
		categoryRepository = new CategoryRepository();
		categoryRepository.em = em;
	}

	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}

	@Test
	public void addCategoryAndFindIt() {
		Long categoryAddedId = commandExecutor.executeCommand(() -> 
			categoryRepository.add(java()).getId()
		);

		assertThat(categoryAddedId, is(notNullValue()));
		final Category category = categoryRepository.findById(categoryAddedId);
		assertThat(category, is(notNullValue()));
		assertThat(category.getName(), is(equalTo(java().getName())));
	}

	@Test
	public void findCategoryByIdNotFound() {
		Category category = categoryRepository.findById(999L);
		assertThat(category, is(nullValue()));
	}

	@Test
	public void findCategoryByIdWithNullId() {
		Category category = categoryRepository.findById(null);
		assertThat(category, is(nullValue()));
	}

	@Test
	public void updateCategory() {
		Long categoryId = commandExecutor.executeCommand(() -> 
			 categoryRepository.add(java()).getId()
		);

		Category categoryAfterAdd = categoryRepository.findById(categoryId);
		assertThat(categoryAfterAdd.getName(), is(equalTo(java().getName())));

		categoryAfterAdd.setName(cleanCode().getName());
		commandExecutor.executeCommand(() -> {
			categoryRepository.update(categoryAfterAdd);
			return null;
		});

		Category categoryAfterUpdate = categoryRepository.findById(categoryId);
		assertThat(categoryAfterUpdate.getName(), is(equalTo(cleanCode().getName())));
	}

	@Test
	public void findAllCategories() {
		commandExecutor.executeCommand(() -> {
			allCategories().forEach(categoryRepository::add);
			return null;
		});

		List<Category> result = categoryRepository.findAll("name");
		assertThat(result, hasSize(allCategories().size()));
		assertThat(result.get(0).getName(), is(equalTo(architecture().getName())));
		assertThat(result.get(1).getName(), is(equalTo(cleanCode().getName())));
		assertThat(result.get(2).getName(), is(equalTo(java().getName())));
		assertThat(result.get(3).getName(), is(equalTo(networks().getName())));
	}

	@Test
	public void alreadyExistsForAdd() {
		Category java = commandExecutor.executeCommand(() -> {
			categoryRepository.add(cleanCode());
			return categoryRepository.add(java());
		});

		assertThat(categoryRepository.alreadyExists(java), is(equalTo(false)));

		java.setName(cleanCode().getName());
		assertThat(categoryRepository.alreadyExists(java), is(equalTo(true)));

		java.setName(networks().getName());
		assertThat(categoryRepository.alreadyExists(java), is(equalTo(false)));
	}

	@Test
	public void alreadyExistWithId() {
		Long categoryAddedId = commandExecutor.executeCommand(() -> 
			categoryRepository.add(java()).getId()
		);
		assertThat(categoryRepository.existsById(categoryAddedId), is(equalTo(true)));
		assertThat(categoryRepository.existsById(999L), is(equalTo(false)));
	}

}