package com.library.app.category.resource;

import static com.library.app.commontests.category.CategoryForTestsRepository.*;
import static org.mockito.Mockito.*;

import java.awt.datatransfer.*;
import java.util.*;

import static com.library.app.commontests.utils.FileTestNameUtils.*;
import static com.library.app.commontests.utils.JsonTestUtils.*;

import javax.inject.*;
import javax.ws.rs.core.*;

import org.apache.commons.codec.binary.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.hamcrest.*;
import org.junit.*;
import org.junit.runner.*;
import org.mockito.*;
import org.mockito.runners.*;
import org.slf4j.*;

import com.google.common.base.*;
import com.library.app.category.exception.*;
import com.library.app.category.model.*;
import com.library.app.category.services.*;
import com.library.app.cetegory.resource.*;
import com.library.app.common.exception.*;
import com.library.app.commontests.utils.*;

import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CaregoryResourceUTest {
	private static String PATH_RESOURCE = ResourceDefinitions.CATEGORY.getResourceName();
	@InjectMocks
	private CategoryResource categoryResource;

	@Mock
	private CategoryServices categoryServices;

	@Spy
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Spy
	private CategoryJsonConverter categoryJsonConverter;

	@Test
	public void addValidCategory() {
		when(categoryServices.add(java())).thenReturn(getCategoryWithId(java(), 1L));

		Response response = categoryResource.add(readJsonFile(getPathFileRequest(PATH_RESOURCE, "newCategory.json")));
		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_CREATED)));
		assertJsonMatchesExpectedJson(response.getEntity().toString(), "{\"id\": 1}");
	}

	@Test
	public void addExistentCategory() {
		when(categoryServices.add(java())).thenThrow(new CategoryExistentException());

		Response response = categoryResource.add(readJsonFile(getPathFileRequest(PATH_RESOURCE, "newCategory.json")));

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryAlreadyExist.json");
	}

	@Test
	public void addCategoryWithNullName() {
		when(categoryServices.add(new Category())).thenThrow(new FieldNotValidException("name", "may not be null"));

		Response response = categoryResource
				.add(readJsonFile(getPathFileRequest(PATH_RESOURCE, "categoryWithNullName.json")));

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryErrorNullName.json");
	}

	@Test
	public void updateValidCategory() {

		Response response = categoryResource.update(1L,
				readJsonFile(getPathFileRequest(PATH_RESOURCE, "category.json")));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
		assertThat(response.getEntity().toString(), is(equalTo(StringUtils.EMPTY)));

		verify(categoryServices).update(getCategoryWithId(java(), 1L));

	}

	@Test
	public void updateCategoryWithNameOfOtherCategory() {
		doThrow(new CategoryExistentException()).when(categoryServices).update(getCategoryWithId(java(), 1L));
		Response response = categoryResource.update(1L,
				readJsonFile(getPathFileRequest(PATH_RESOURCE, "category.json")));

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryAlreadyExist.json");
	}

	@Test
	public void updateCategoryWithNullName() {
		doThrow(new FieldNotValidException("name", "may not be null")).when(categoryServices)
				.update(Mockito.any(Category.class));
		Response response = categoryResource.update(1L,
				readJsonFile(getPathFileRequest(PATH_RESOURCE, "categoryWithNullName.json")));

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_UNPROCESSABLE_ENTITY)));
		assertJsonResponseWithFile(response, "categoryErrorNullName.json");
	}

	@Test
	public void updateCategoryNotFound() {
		doThrow(new CategoryNotFoundException()).when(categoryServices).update(Mockito.any(Category.class));
		Response response = categoryResource.update(2L,
				readJsonFile(getPathFileRequest(PATH_RESOURCE, "categoryWithNullName.json")));

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_NOT_FOUND)));
		assertJsonResponseWithFile(response, "categoryNotFound.json");
	}

	@Test
	public void findById() {
		when(categoryServices.findById(anyLong())).thenReturn(getCategoryWithId(java(), 1L));
		Response response = categoryResource.findById(1L);

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_OK)));
		assertJsonResponseWithFile(response, "categoryFound.json");
	}

	@Test
	public void findByIdNotFound() {
		doThrow(new CategoryNotFoundException()).when(categoryServices).findById(anyLong());
		Response response = categoryResource.findById(1L);

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_NOT_FOUND)));
		assertJsonResponseWithFile(response, "categoryNotFound.json");
	}
	
	@Test
	public void findAll() {
		List<Category> two = new LinkedList<Category>();
		two.add(getCategoryWithId(java(), 1L));
		two.add(getCategoryWithId(networks(), 2L));
		
		when(categoryServices.findAll()).thenReturn(two);
		Response response = categoryResource.findAll();

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_OK)));
		assertJsonResponseWithFile(response, "twoCategories.json");
	}
	
	@Test
	public void findAllNothingFound() {
		
		when(categoryServices.findAll()).thenReturn(new  ArrayList<Category>());
		Response response = categoryResource.findAll();

		assertThat(response.getStatus(), CoreMatchers.is(CoreMatchers.equalTo(HttpStatus.SC_OK)));
		assertJsonResponseWithFile(response, "emptyListOfCategories.json");
	}

	private void assertJsonResponseWithFile(Response response, String fileName) {
		assertJsonMatchesFileContent(response.getEntity().toString(), getPathFileResponse(PATH_RESOURCE, fileName));
	}
}
