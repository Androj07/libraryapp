package com.library.app.commontests.utils;

import java.io.*;
import java.util.*;

import org.json.*;
import org.junit.*;
import org.skyscreamer.jsonassert.*;

import com.google.gson.*;
import com.library.app.common.json.*;

@Ignore
public class JsonTestUtils {
	public static final String BASE_JSON_DIR = "json/";
	
	private JsonTestUtils(){
		
	}
	
	public static String readJsonFile(String relativePath){
		InputStream is = JsonTestUtils.class.getClassLoader().getResourceAsStream(BASE_JSON_DIR + relativePath);
		try(Scanner s = new Scanner(is)){
			return s.useDelimiter("\\A").hasNext() ? s.next() : "";
		}
	}
	
	public static void assertJsonMatchesFileContent(String actualJson, String fileNameWithExpectedJson){
		assertJsonMatchesExpectedJson(actualJson, readJsonFile(fileNameWithExpectedJson));
	}
	
	public static void assertJsonMatchesExpectedJson(String actualJson, String expectedJson){
			try {
				JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.NON_EXTENSIBLE);
			} catch (JSONException e) {
				throw new IllegalArgumentException();
			}
	}
	
	public static Long getIdFromJson(final String json) {
		final javax.json.JsonObject jsonObject =  JsonReaderUtil.readAsJsonObject(json);
		Long id = jsonObject.getJsonNumber("id").longValue();
		return   id;
	}

}
