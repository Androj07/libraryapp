package com.library.app.commontests.utils;

import org.junit.*;

@Ignore
public class FileTestNameUtils {
	private static final String PATH_REQUEST = "/request/";
	private static final String PATH_RESPONSE = "/response/";
	
	public static String getPathFileRequest(String mainFolder, String name){
		return mainFolder + PATH_REQUEST + name;
	}
	
	public static String getPathFileResponse(String mainFolder, String name){
		return mainFolder+ PATH_RESPONSE + name;
	}

}
