package com.library.app.common.json;

import com.google.gson.*;

public class JsonUtils {
	private JsonUtils(){
		
	}
	
	public static JsonElement getJsonElementWithId(Long id){
		JsonObject idJson = new JsonObject();
		idJson.addProperty("id", id);
		return idJson;
	}
}
