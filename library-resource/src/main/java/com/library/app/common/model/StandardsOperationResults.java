package com.library.app.common.model;

import com.library.app.common.exception.*;

public class StandardsOperationResults {

	private StandardsOperationResults(){
		
	}
	
	public static OperationResult getOperationResultExistent(ResourceMessage resourceMessage, String fieldsName){
		return OperationResult.error(resourceMessage.getKeyOfResourceExistent(), resourceMessage.getMessageOfResourceExistent(fieldsName));
	}
	
	public static OperationResult getOperationResultInvalidField(ResourceMessage resourceMessage, FieldNotValidException ex){
		return OperationResult.error(resourceMessage.getKeyOfInvelidField(ex.getFieldName()), ex.getMessage());
	}
	
	public static OperationResult getOperationResultNotFound(ResourceMessage resourceMessage){
		return OperationResult.error(resourceMessage.getResourceKeyNotFound(), resourceMessage.getMessageOfResourceNotFound());
	}
}
