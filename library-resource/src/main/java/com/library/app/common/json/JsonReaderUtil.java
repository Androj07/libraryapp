package com.library.app.common.json;

import java.io.*;

import javax.json.*;
import javax.json.JsonObject;

import com.google.gson.*;
import com.google.gson.JsonArray;
import com.library.app.cetegory.resource.common.exception.*;

public class JsonReaderUtil {
	
	public static JsonObject readAsJsonObject(final String json) throws InvalidJsonException {
		JsonReader reader = Json.createReader(new StringReader(json));
		JsonObject readObject = reader.readObject();
		reader.close();
		return readObject;
	}

	public static JsonArray readAsJsonArray(final String json) throws InvalidJsonException {
		return readJsonAs(json, JsonArray.class);
	}

	public static <T> T readJsonAs(final String json, final Class<T> jsonClass) throws InvalidJsonException {
		if (json == null || json.trim().isEmpty()) {
			throw new InvalidJsonException("Json String can not be null");
		}
		try {
			return new Gson().fromJson(json, jsonClass);
		} catch (final JsonSyntaxException e) {
			throw new InvalidJsonException(e);
		}
	}
}
