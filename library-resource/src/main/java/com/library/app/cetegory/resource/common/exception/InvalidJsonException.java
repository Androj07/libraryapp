package com.library.app.cetegory.resource.common.exception;

public class InvalidJsonException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public InvalidJsonException(String message) {
		super(message);
	}

	public InvalidJsonException(Throwable cause) {
		super(cause);
	}
	
	

}
