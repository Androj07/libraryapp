package com.library.app.cetegory.resource;

import static com.library.app.common.model.StandardsOperationResults.*;

import java.util.*;

import javax.inject.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.http.*;
import org.slf4j.*;

import com.google.gson.*;
import com.library.app.category.exception.*;
import com.library.app.category.model.*;
import com.library.app.category.model.Category;
import com.library.app.category.services.*;
import com.library.app.common.exception.*;
import com.library.app.common.json.*;
import com.library.app.common.model.*;
import org.jboss.weld.log.*;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoryResource {
	private static final ResourceMessage RESOURCE_MESSAGE = new ResourceMessage("category");

	@Inject
	private CategoryServices categoryServices;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	private CategoryJsonConverter categoryJsonConverter;

	@POST
	public Response add(String body) {
		logger.debug("Adding new category with body {0}", body);
		Category category = categoryJsonConverter.convertFrom(body);

		OperationResult result = null;
		int status = HttpStatus.SC_CREATED;
		try {
			category = categoryServices.add(category);
			result = OperationResult.success(JsonUtils.getJsonElementWithId(category.getId()));
		} catch (FieldNotValidException e) {
			logger.error("Field not valid exception ", e);
			status = HttpStatus.SC_UNPROCESSABLE_ENTITY;
			result = getOperationResultInvalidField(RESOURCE_MESSAGE, e);
		} catch (CategoryExistentException e) {
			logger.error("There is already category with this name ", e);
			status = HttpStatus.SC_UNPROCESSABLE_ENTITY;
			result = getOperationResultExistent(RESOURCE_MESSAGE, "name");
		}

		logger.debug("Returninug the operation result after adding category: {0}", result);
		return Response.status(status).entity(OperationResultJsonWriter.toJson(result)).build();
	}
	@PUT
	@Path("/{id}")
	public Response update(@PathParam("id") Long id, String body) {
		logger.debug("Updating category {0} with body {1}", id, body);
		Category category = categoryJsonConverter.convertFrom(body);
		category.setId(id);

		OperationResult result = null;
		int status = HttpStatus.SC_OK;
		try {
			categoryServices.update(category);
			result = OperationResult.success();
		} catch (FieldNotValidException e) {
			logger.error("Field not valid exception ", e);
			status = HttpStatus.SC_UNPROCESSABLE_ENTITY;
			result = getOperationResultInvalidField(RESOURCE_MESSAGE, e);
		} catch (CategoryExistentException e) {
			logger.error("There is already category with this name ", e);
			status = HttpStatus.SC_UNPROCESSABLE_ENTITY;
			result = getOperationResultExistent(RESOURCE_MESSAGE, "name");
		} catch (CategoryNotFoundException e) {
			logger.error("There is already category with this name ", e);
			status = HttpStatus.SC_NOT_FOUND;
			result = getOperationResultNotFound(RESOURCE_MESSAGE);
		}

		logger.debug("Returninug the operation result after adding category: {0}", result);
		return Response.status(status).entity(OperationResultJsonWriter.toJson(result)).build();
	}

	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") Long id) {
		logger.debug("Search categoy with id {0}", id);

		OperationResult result = null;
		int status = HttpStatus.SC_OK;
		try {
			Category category = categoryServices.findById(id);
			result = OperationResult.success(category);
			logger.debug("Found category {0}", category);
		} catch (CategoryNotFoundException e) {
			logger.error("Category not found", e);
			status = HttpStatus.SC_NOT_FOUND;
			result = getOperationResultNotFound(RESOURCE_MESSAGE);
		}
		logger.debug("Returninug the operation result after search category: {0}", result);
		return Response.status(status).entity(OperationResultJsonWriter.toJson(result)).build();
	}
	@GET
	public Response findAll() {
		logger.debug("Find all");
 
		List<Category> all = categoryServices.findAll();
		JsonElement jsonWithPagingAndEntries = getJsonElementWithPagingAndEntries(all);
		int status = HttpStatus.SC_OK;
		return Response.status(status).entity(JsonWriter.writeToString(jsonWithPagingAndEntries)).build();
	}

	private JsonElement getJsonElementWithPagingAndEntries(List<Category> categories) {
		JsonObject jObject = new JsonObject();
		
		JsonObject jPaging = new JsonObject();
		jPaging.addProperty("totalRecords", categories.size());
		
		jObject.add("paging", jPaging);
		jObject.add("entries", categoryJsonConverter.convertToJsonElement(categories));
		
		return jObject;
	}
}
