package com.library.app.cetegory.resource;

import java.io.*;
import java.util.*;

import javax.enterprise.context.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.*;
import com.google.gson.*;
import com.library.app.category.model.*;
import com.library.app.cetegory.resource.common.exception.*;

@ApplicationScoped
public class CategoryJsonConverter {
	public Category convertFrom(String json){
		ObjectMapper mapper = new ObjectMapper();
		Category category = new Category();
		try {
			category = mapper.readValue(json, Category.class);
		} catch (JsonParseException e) {
			throw new InvalidJsonException(e);
		} catch (JsonMappingException e) {
			throw new InvalidJsonException(e);
		} catch (IOException e) {
			throw new InvalidJsonException(e);
		}
		return category;
	}

	public JsonElement convertToJsonElement(List<Category> categories) {
		JsonArray jArray = new JsonArray();
		for(Category c : categories){
			JsonObject jObject = new JsonObject();
			jObject.addProperty("id", c.getId());
			jObject.addProperty("name", c.getName());
			jArray.add(jObject);
		}
		
		return jArray;
	}
}
